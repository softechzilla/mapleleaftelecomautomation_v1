<?php

/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package linoor
 */

?>


<?php
$linoor_page_id     = get_queried_object_id();
$linoor_custom_footer_status = !empty(get_post_meta($linoor_page_id, 'linoor_custom_footer_status', true)) ? get_post_meta($linoor_page_id, 'linoor_custom_footer_status', true) : 'off';

?>



<?php if ('on' == $linoor_custom_footer_status && true == post_type_exists('footer') && is_page()) : ?>
	<?php echo do_shortcode('[linoor-footer id="' . get_post_meta($linoor_page_id, 'linoor_select_custom_footer', true) . '"]');
	?>


<?php else : ?>
	<?php if (true == get_theme_mod('footer_custom', false) && true == post_type_exists('footer')) : ?>

		<?php echo do_shortcode('[linoor-footer id="' . get_theme_mod('footer_custom_post') . '"]');
		?>




	<?php else : ?>

		<!-- Main Footer -->
		<footer class="main-footer normal-padding">
			<!-- Footer Bottom -->
			<div class="footer-bottom">
				<div class="auto-container">
					<div class="inner clearfix">
						<?php $linoor_footer_copyright = get_theme_mod('footer_copytext', '&copy; All right reserved'); ?>
						<div class="copyright"><?php echo wp_kses($linoor_footer_copyright, 'linoor_allowed_tags'); ?></div>
					</div>
				</div>
			</div>
		</footer>


	<?php endif; ?>


<?php endif; ?>



</div><!-- #page -->

</div><!-- /.page-wrapper -->



<!--Mobile Menu-->
<div class="side-menu__block">
	<div class="side-menu__block-overlay">
	</div><!-- /.side-menu__block-overlay -->
	<div class="side-menu__block-inner ">
		<div class="side-menu__top justify-content-between">
			<div class="logo">
				<a href="<?php echo esc_url(home_url('/')); ?>">
					<?php
					$linoor_logo_size = get_theme_mod('header_logo_width', 133);
					$linoor_custom_logo_id = get_theme_mod('custom_logo');
					$linoor_logo = wp_get_attachment_image_src($linoor_custom_logo_id, 'full');
					if (has_custom_logo()) {
						echo '<img width="' . esc_attr($linoor_logo_size) . '" src="' . esc_url($linoor_logo[0]) . '" alt="' . esc_attr(get_bloginfo('name')) . '">';
					} else {
						echo '<h1>' . esc_html(get_bloginfo('name')) . '</h1>';
					} ?>
				</a>
			</div>
			<a href="#" class="side-menu__toggler side-menu__close-btn"></a>
		</div><!-- /.side-menu__top -->


		<nav class="mobile-nav__container">
			<!-- content is loading via js -->
		</nav>
		<?php $linoor_mobile_menu_content = get_theme_mod('linoor_mobile_menu_text'); ?>
		<?php if (!empty($linoor_mobile_menu_content)) : ?>
			<div class="side-menu__sep"></div><!-- /.side-menu__sep -->
		<?php endif; ?>
		<div class="side-menu__content">
			<?php if (!empty($linoor_mobile_menu_content)) : ?>
				<?php echo wp_kses($linoor_mobile_menu_content, 'linoor_allowed_tags'); ?>
			<?php endif; ?>
			<div class="side-menu__social">
				<?php $linoor_mobile_menu_social = get_theme_mod('mobile_menu_social_icons'); ?>
				<?php if (!empty($linoor_mobile_menu_social)) : ?>
					<?php foreach ($linoor_mobile_menu_social as $social_icon) : ?>
						<a href="<?php echo esc_url($social_icon['link_url']); ?>"><i class="fab <?php echo esc_attr($social_icon['link_icon']); ?>"></i></a>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div><!-- /.side-menu__content -->
	</div><!-- /.side-menu__block-inner -->
</div><!-- /.side-menu__block -->


<?php $linoor_back_to_top_status = get_theme_mod('scroll_to_top', false); ?>
<?php if (true === $linoor_back_to_top_status) : ?>
	<a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa <?php echo esc_attr(get_theme_mod('scroll_to_top_icon', 'fa-angle-up')); ?>"></i></a>

<?php endif; ?>



<?php wp_footer(); ?>

</body>

</html>