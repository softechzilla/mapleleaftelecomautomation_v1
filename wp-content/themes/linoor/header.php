<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package linoor
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<?php wp_body_open(); ?>

	<?php $linoor_preloader_status = get_theme_mod('preloader', false); ?>
	<?php if (true === $linoor_preloader_status) : ?>
		<!-- Preloader -->
		<div class="preloader">
			<div class="icon"></div>
		</div>
	<?php endif; ?>

	<?php
	$linoor_get_boxed_wrapper_status = get_theme_mod('linoor_boxed_layout', false);
	$linoor_dynamic_boxed_wrapper_status = isset($_GET['boxed_status']) ? $_GET['boxed_status'] : $linoor_get_boxed_wrapper_status;


	?>

	<div class="page-wrapper <?php echo esc_attr((true == $linoor_dynamic_boxed_wrapper_status) ? 'boxed-wrapper' : ''); ?>">
		<div id="page" class="site">


			<!-- Main Header -->
			<?php $linoor_sticky_menu_class = get_theme_mod('header_stricked_menu', true) && !is_admin_bar_showing() ? 'sticky-menu' : ''; ?>
			<header class="main-header header-style-one <?php echo esc_attr($linoor_sticky_menu_class); ?>">

				<!-- Header Upper -->
				<div class="header-upper">
					<div class="inner-container clearfix">
						<!--Logo-->
						<div class="logo-box">
							<div class="logo">
								<a href="<?php echo esc_url(home_url('/')); ?>">
									<?php
									$linoor_logo_size = get_theme_mod('header_logo_width', 133);
									$linoor_custom_logo_id = get_theme_mod('custom_logo');
									$linoor_logo = wp_get_attachment_image_src($linoor_custom_logo_id, 'full');
									if (has_custom_logo()) {
										echo '<img width="' . esc_attr($linoor_logo_size) . '" src="' . esc_url($linoor_logo[0]) . '" alt="' . esc_attr(get_bloginfo('name')) . '">';
									} else {
										echo '<h1>' . esc_html(get_bloginfo('name')) . '</h1>';
									} ?>
								</a>
							</div>
						</div>
						<div class="nav-outer clearfix">
							<!--Mobile Navigation Toggler-->
							<div class="mobile-nav-toggler"><span class="icon flaticon-menu-2"></span><span class="txt"><?php esc_html_e('Menu', 'linoor'); ?></span></div>

							<!-- Main Menu -->
							<nav class="main-menu navbar-expand-md navbar-light">
								<div class="collapse navbar-collapse show clearfix" id="navbarSupportedContent">
									<?php
									wp_nav_menu(
										array(
											'theme_location' => 'menu-1',
											'menu_id'        => 'primary-menu',
											'fallback_cb' => 'linoor_menu_fallback_callback',
											'menu_class' => 'navigation clearfix',
										)
									);
									?>
								</div>
							</nav>
						</div>

						<div class="other-links clearfix">
							<!--Search Btn-->
							<?php $linoor_search_btn_status = get_theme_mod('header_search_btn', false); ?>
							<?php if ($linoor_search_btn_status) : ?>
								<div class="search-btn">
									<button type="button" class="theme-btn search-toggler"><span class="flaticon-loupe"></span></button>
								</div>
							<?php endif; ?>

							<?php $linoor_call_btn_status = get_theme_mod('header_call_btn_switch', false); ?>
							<?php if ($linoor_call_btn_status) : ?>
								<div class="link-box">
									<div class="call-us">
										<a class="link" href="<?php echo esc_url(get_theme_mod('header_call_btn_link')); ?>">
											<span class="icon"></span>
											<span class="sub-text"><?php echo esc_html(get_theme_mod('header_call_btn_text')); ?></span>
											<span class="number"><?php echo esc_html(get_theme_mod('header_call_btn_number')); ?></span>
										</a>
									</div>
								</div>
							<?php endif; ?>
						</div>

					</div>
				</div>
				<!--End Header Upper-->


			</header>
			<!-- End Main Header -->


			<!--Search Popup-->
			<div class="search-popup">
				<div class="search-popup__overlay">
				</div><!-- /.search-popup__overlay -->
				<div class="search-popup__inner">
					<?php echo get_search_form(); ?>
				</div><!-- /.search-popup__inner -->
			</div><!-- /.search-popup -->


			<?php $linoor_page_header_status = empty(get_post_meta(get_the_ID(), 'linoor_show_page_banner', true)) ? 'on' : get_post_meta(get_the_ID(), 'linoor_show_page_banner', true);  ?>

			<?php if (is_page() && 'on' === $linoor_page_header_status) : ?>
				<?php get_template_part('template-parts/page-header'); ?>
			<?php elseif (!is_page()) :  ?>
				<?php get_template_part('template-parts/page-header'); ?>
			<?php endif; ?>