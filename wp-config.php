<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mapleleaftelecomautomation_v1' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'jj3ZWSK]+2+A2F5(4?cW5z,^Tip_} Y[j|4HqN1m$zTi>|z?)R@Rtg}?tzQVGRp>' );
define( 'SECURE_AUTH_KEY',  'LtHtE360;(]I{iVQ-xa6jo$MPkZdJy^?&~*fU )j_3>>iYGGlzDEc&L_?6?yU1M0' );
define( 'LOGGED_IN_KEY',    'QS,&zRf=/B7hT<ttXi6Y^[heYiZVWT9~Z8~)rAw|[^e-mQ;[0KMA1nkx1<BVj8/v' );
define( 'NONCE_KEY',        'nPDW5YxqoHjj0?l)1)E{a,K9jyjT=c)jYO4|[%$!K>fa{$}>QBdxQ5%;5i/_QCCz' );
define( 'AUTH_SALT',        '@]n3Yhrr AXoR-YGr{k*H3EFyI$pFeI&6> x/K(I{b&ga6}sW_9w]U{v5xN<QS9A' );
define( 'SECURE_AUTH_SALT', 'Kj,tvFuce[F/}8j-|!kr~ec0l6%Ehdo9`fJ4px>MyUYml:@KXIXV>A[0:S:{UpQ@' );
define( 'LOGGED_IN_SALT',   '-h`PTvPwwl/21`AC,G|k9bG!u)Q5F0:!]d|Imx29Mn?&I35zF)@n2WhH+5duFqnC' );
define( 'NONCE_SALT',       'Hbun<^wu4sMdExG>n^^2m2ap_KGr#/~1~Vn2w/Zz2zijX1UQ@26yF)p?ZAr,zqbx' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
